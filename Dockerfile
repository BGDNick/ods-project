FROM mambaorg/micromamba

WORKDIR /app
COPY env.yml /app/env.yml
RUN micromamba create -f env.yml
COPY pyproject.toml /app/pyproject.toml
RUN micromamba run -n gitlabproject poetry install
